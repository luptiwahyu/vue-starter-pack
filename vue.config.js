const path = require('path')

module.exports = {
  devServer: {
    port: 2000,
    publicPath: '/',
    contentBase: path.join(__dirname, 'static'),
    open: true
  }
}
