import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// External Package
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'sui-icon/icon.css'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

// Internal Package
import './assets/css/main.scss'
import './assets/css/themes/default.scss'

Vue.use(ElementUI)

Vue.config.productionTip = false

const requireComponent = require.context(
  '@/components/atomic', true, /[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)
  const componentName = upperFirst(
    camelCase(fileName.split('/').pop().replace(/\.\w+$/, ''))
  )
  Vue.component(componentName, componentConfig.default || componentConfig)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
